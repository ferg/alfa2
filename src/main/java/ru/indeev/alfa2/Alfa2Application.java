package ru.indeev.alfa2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Alfa2Application {

	public static void main(String[] args) {
		SpringApplication.run(Alfa2Application.class, args);
	}

}
